from django.shortcuts import render, get_object_or_404
from django.http.response import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from main.decorators import check_mode, ajax_required
from main.functions import get_auto_id, generate_form_errors, get_a_id
import json
from django.views.decorators.http import require_GET
from users.models import NotificationSubject, Notification
from django.db.models import Sum
from django.contrib.auth.models import Group
import datetime
from calendar import monthrange
from django.utils import timezone
from decimal import Decimal
from customers.models import Customer
from customers.forms import CustomerForm


def create(request):
    if request.method == "POST":
        form = CustomerForm(request.POST)
        if form.is_valid():
            data = form.save(commit = False)
            # curent user
            data.creator = request.user 
            data.updator = request.user 
            data.auto_id = get_auto_id(Customer)
            data.save()

            response_data = {
                'status' : 'true',
                'title' : 'succesfully submitted',
                'message' : 'registration completed succesfully',
                'redirect' : 'true',
                'redirect_url' : reverse('customers:customer',kwargs = {"pk" :data.pk}) 
            }

        else:
            message = generate_form_errors(form,formset=False)

            response_data = {
                'status' : 'false',
                'stable' : 'true',
                'title' : 'form validation error',
                'message' : message,
            }

        return HttpResponse(json.dumps(response_data),content_type = 'application/javascript')
    else:
        form = CustomerForm()
        context = {
            "form" : form,
            "title" : "create customer",
            "redirect" : True,

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
            "is_need_animations": True,

        }

        return render(request,'customers/entry.html',context)


def customers(request):
    instances = Customer.objects.filter(is_deleted = False)
    context = {
        "title" : "Customers",
        "instances" : instances,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
        "is_need_animations": True,
    }

    return render(request, "customers/customers.html" ,context)


def edit(request,pk):
    instance = get_object_or_404(Customer.objects.filter(pk = pk))
    if request.method == "POST":
        form = CustomerForm(request.POST,instance = instance)
        if form.is_valid():
            data = form.save(commit = False)
            data.date_updated = datetime.datetime.now()
            data.updator = request.user 
            data.auto_id = get_auto_id(Customer)
            data.save()

            response_data = {
                'status' : 'true',
                'title' : 'succesfully Updated',
                'message' : 'registration completed succesfully',
                'redirect' : 'true',
                'redirect_url' : reverse('customers:customer',kwargs = {"pk" :data.pk}) 
            }

        else:
            message = generate_form_errors(form,formset=False)

            response_data = {
                'status' : 'false',
                'stable' : 'true',
                'title' : 'form validation error',
                'message' : message,
            }

        return HttpResponse(json.dumps(response_data),content_type = 'application/javascript')
    else:
        form = CustomerForm(instance = instance)
        context = {
            "form" : form,
            "title" : "create customer",
            "redirect" : True,

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
            "is_need_animations": True,

        }

        return render(request,'customers/entry.html',context)


def delete(request,pk):
    Customer.objects.filter(pk=pk).update(is_deleted=True)

    response_data = {
        "status" : "true",
        "title" : "Successfully Deleted",
        "message" : "Customer Successfully Deleted.",
        "redirect" : "true",
        "redirect_url" : reverse('customers:customers')
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')


def customer(request,pk):
    instance = get_object_or_404(Customer.objects.filter(pk = pk))
    context = {
        "title" : "Customer "+instance.name,
        "instance" : instance,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
        "is_need_animations": True,
    }

    return render(request, "customers/customer.html" ,context)