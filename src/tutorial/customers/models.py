from __future__ import unicode_literals
from django.db import models
from main.models import BaseModel
from django.utils.translation import ugettext_lazy as _


class Customer(BaseModel):
    name = models.CharField(max_length = 120)
    phone = models.CharField(max_length = 12)
    email = models.EmailField()
    address = models.TextField()

    class Meta:
        db_table = 'customers_customer'
        verbose_name = _('customer')
        verbose_name_plural = _('customers')
        ordering = ('-date_added','name')

    def __unicode__(self):
        return str(self.name)
