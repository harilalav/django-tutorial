from django.conf.urls import url,include
import views


urlpatterns = [
    url(r'^create/$', views.create, name='create'),
    url(r'^$', views.customers, name='customers'),
    url(r'^edit/(?P<pk>.*)$', views.edit, name='edit'),
    url(r'^delete/(?P<pk>.*)$', views.delete, name='delete'),
    url(r'^view/(?P<pk>.*)$', views.customer, name='customer'),
]