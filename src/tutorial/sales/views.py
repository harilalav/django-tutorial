from django.shortcuts import render, get_object_or_404
from django.http.response import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from main.decorators import check_mode, ajax_required
from main.functions import get_auto_id, generate_form_errors, get_a_id
import json
from django.views.decorators.http import require_GET
from users.models import NotificationSubject, Notification
from django.db.models import Sum
from django.contrib.auth.models import Group
import datetime
from calendar import monthrange
from django.utils import timezone
from decimal import Decimal
from sales.models import Sale
from sales.forms import SaleForm,SaleItemForm
from django.forms import formset_factory


def create(request):
    SaleItemFormset = formset_factory(SaleItemForm,extra = 1)
    if request.method == "POST":
        form = SaleForm(request.POST)
        sale_item_formset = SaleItemFormset(request.POST, prefix = "sale_item_formset")
        if form.is_valid() and sale_item_formset.is_valid():
            customer = form.cleaned_data['customer']
            data = form.save(commit   = False)
            data.creator = request.user
            data.updater = request.user
            data.auto_id = get_auto_id(Sale)
            data.save()

            for item in sale_item_formset:
                product = item.cleaned_data('product')
                qty = item.cleaned_data('qty')

                SaleItem.object.create(
                    product = product,
                    qty = qty,
                    sale = data
                    )

            response_data = {
                "status" : "true",
                "title" : "Successfully created",
                "message" : "Sale Successfully created.",
                "redirect" : "true",
                "redirect_url" : reverse('sales:sales', kwargs = {'pk':data.pk})
            }
        else:
            
            message = generate_form_errors(form,formset = False)
            message += generate_form_errors(sale_item_formset,formset = True)

            response_data = {
                "status" : "true",
                "title" : "Form Validation Error",
                "message" : message,
            }

        return HttpResponse(json.dumps(response_data), content_type='application/javascript')

    else:
        form = SaleForm()
        sale_item_formset = SaleItemFormset(prefix = "sale_item_formset")
        context = {
            "form" : form,
            "sale_item_formset" : sale_item_formset,
            "title" : "Sale Entry",
            "url" : reverse('sales:create'),
            "redirect" : True,
            "is_create_page" : True,

            "is_need_select_picker" : True,
            "is_need_popup_box" : True,
            "is_need_custom_scroll_bar" : True,
            "is_need_wave_effect" : True,
            "is_need_bootstrap_growl" : True,
            "is_need_chosen_select" : True,
            "is_need_grid_system" : True,
            "is_need_datetime_picker" : True,
        }

        return render(request,'sales/entry.html',context)

    
def edit(request,pk):
    pass


def sale(request,pk):
    instance = get_object_or_404(Sale.objects.filter(pk = pk))
    context = {
        "title" : "Sale : "+str(instance.auto_id),
        "instance" : instance,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
        "is_need_animations": True,
    }

    return render(request, "sales/sale.html" ,context)


def sales(request):
    instances = Sale.objects.filter(is_deleted = False)
    context = {
        "title" : "Sales",
        "instances" : instances,

        "is_need_select_picker" : True,
        "is_need_popup_box" : True,
        "is_need_custom_scroll_bar" : True,
        "is_need_wave_effect" : True,
        "is_need_bootstrap_growl" : True,
        "is_need_chosen_select" : True,
        "is_need_grid_system" : True,
        "is_need_datetime_picker" : True,
        "is_need_animations": True,
    }

    return render(request, "sales/sales.html" ,context)


def delete(request,pk):
    Sale.objects.filter(pk=pk).update(is_deleted=True)

    response_data = {
        "status" : "true",
        "title" : "Successfully Deleted",
        "message" : "Sale Successfully Deleted.",
        "redirect" : "true",
        "redirect_url" : reverse('sales:sales')
    }
    return HttpResponse(json.dumps(response_data), content_type='application/javascript')