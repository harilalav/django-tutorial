from __future__ import unicode_literals
from django.db import models
from main.models import BaseModel
from products.models import Product
from django.core.validators import MinValueValidator
from decimal import Decimal
import uuid
from django.utils.translation import ugettext_lazy as _


class Sale(BaseModel):
    customer = models.ForeignKey('customers.Customer',limit_choices_to = {"is_deleted" : False})
    date = models.DateField()
    subtotal = models.DecimalField(default = 0.0, decimal_places = 2, max_digits = 15, validators= [MinValueValidator(Decimal('0.00'))])
    discount = models.DecimalField(default = 0.0, decimal_places = 2, max_digits = 15, validators= [MinValueValidator(Decimal('0.00'))])
    total = models.DecimalField(default = 0.0, decimal_places = 2, max_digits = 15, validators= [MinValueValidator(Decimal('0.00'))])

    class Meta(object):
        db_table = 'sales_sale'
        verbose_name = _('sale')
        verbose_name_plural = _('sales')
        ordering = ('-date',)

    def __unicode__(self):
        return str(self.auto_id)

class SaleItem(BaseModel):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    sale = models.ForeignKey('sales.Sale')
    product = models.ForeignKey('products.Product')
    qty = models.DecimalField(default = 0.0, decimal_places = 2, max_digits = 15, validators= [MinValueValidator(Decimal('0.00'))])

    class Meta(object):
        db_table = 'sales_saleItem'
        verbose_name = _('saleItem')
        verbose_name_plural = _('saleItems')
        ordering = ('-sale',)

    def __unicode__(self):
        return str(self.sale.auto_id)